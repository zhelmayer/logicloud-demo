package hu.logicloud.demo.models.items.domain;

import hu.logicloud.demo.models.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Collection;

@Data
@Audited
@Entity(name = "ItemType")
@Table(name = "item_types")
@EqualsAndHashCode(callSuper=false)
public class ItemType extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @OneToMany(mappedBy="itemType", fetch=FetchType.LAZY)
    private Collection<Item> items;
}
