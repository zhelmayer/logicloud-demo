package hu.logicloud.demo.models.items.domain;

import hu.logicloud.demo.models.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Data
@Audited
@Entity
@Table(name = "items")
@EqualsAndHashCode(callSuper=false)
public class Item extends BaseEntity {

    @Id
    @Column(name = "code")
    private String code;

    @Column(name = "name", nullable = false)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "type_id", foreignKey = @ForeignKey(name = "fk_items_2_item_types"))
    private ItemType itemType;
}
