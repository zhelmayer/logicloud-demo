package hu.logicloud.demo.models.users.domain;

import hu.logicloud.demo.models.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Data
@Audited
@Entity
@Table(name = "user_settings")
@EqualsAndHashCode(callSuper=false)
public class UserSettings extends BaseEntity {

    @EmbeddedId
    private UserSettingsId userSettingsId;

    @Column(name = "value")
    private String value;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "fk_user_settings_2_users"), insertable = false, updatable = false)
    private User user;
}
