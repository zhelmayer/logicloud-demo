package hu.logicloud.demo.models.listeners;

import hu.logicloud.demo.models.BaseEntity;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.util.Date;

public class AuditListener {

    @PrePersist
    public void onPrePersist(BaseEntity entity) {
        Date now = new Date();
        entity.setCreatedDate(now);
        entity.setUpdatedDate(now);
    }

    @PreUpdate
    public void onPreUpdate(BaseEntity entity) {
        entity.setUpdatedDate(new Date());
    }
}
