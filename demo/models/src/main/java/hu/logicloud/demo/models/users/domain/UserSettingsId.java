package hu.logicloud.demo.models.users.domain;

import lombok.*;
import org.hibernate.envers.Audited;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
@Audited
public class UserSettingsId implements Serializable {

    @Column(name = "code")
    private String code;

    @Column(name = "user_id")
    private Long userId;
}
