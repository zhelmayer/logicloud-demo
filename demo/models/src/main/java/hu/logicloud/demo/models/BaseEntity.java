package hu.logicloud.demo.models;

import hu.logicloud.demo.models.listeners.AuditListener;
import lombok.Data;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;

@Data
@Audited
@MappedSuperclass
@EntityListeners(AuditListener.class)
public abstract class BaseEntity {

    @Version
    @Column(name = "version")
    private Long version;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date", nullable = false, updatable = false)
    private Date createdDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_date", nullable = false)
    private Date updatedDate;
}
