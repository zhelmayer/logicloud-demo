create sequence hibernate_sequence;

CREATE TABLE users
(
    id bigserial constraint pk_users primary key,
    login varchar NOT NULL UNIQUE,
    name varchar,
    version bigint,
    created_date timestamp without time zone NOT NULL,
    updated_date timestamp without time zone NOT NULL
);

CREATE TABLE user_settings
(
    code varchar,
    user_id bigint constraint fk_user_settings_2_users references users,
    value varchar,
    version bigint,
    created_date timestamp without time zone NOT NULL,
    updated_date timestamp without time zone NOT NULL,
    constraint pk_user_settings primary key(code, user_id)
);

CREATE TABLE revinfo
(
    rev integer not null constraint pk_revinfo primary key,
    revtstmp bigint
);

CREATE TABLE users_aud
(
    id bigint NOT NULL,
    rev integer NOT NULL constraint fk_users_aud_2_revinfo references revinfo,
    revtype smallint,
    login varchar(255),
    name varchar(255),
    version bigint,
    created_date timestamp without time zone,
    updated_date timestamp without time zone,
    constraint pk_users_aud primary key (id, rev)
);

CREATE TABLE user_settings_aud
(
    code varchar(255) NOT NULL,
    user_id bigint,
    rev integer NOT NULL constraint fk_user_settings_aud_2_revinfo references revinfo,
    revtype smallint,
    value varchar(255),
    version bigint,
    created_date timestamp without time zone,
    updated_date timestamp without time zone,
    constraint pk_user_settings_aud primary key (code, user_id, rev)
);

