create sequence hibernate_sequence;

CREATE TABLE item_types
(
    id bigserial constraint pk_item_types primary key,
    name varchar NOT NULL UNIQUE,
    version bigint,
    created_date timestamp without time zone NOT NULL,
    updated_date timestamp without time zone NOT NULL
);

CREATE TABLE items
(
    code varchar constraint pk_items primary key,
    type_id bigint NOT NULL constraint fk_items_2_item_types references item_types,
    name varchar NOT NULL UNIQUE,
    version bigint,
    created_date timestamp without time zone NOT NULL,
    updated_date timestamp without time zone NOT NULL
);

CREATE TABLE revinfo
(
    rev integer not null constraint pk_revinfo primary key,
    revtstmp bigint
);

CREATE TABLE item_types_aud
(
    id bigint NOT NULL,
    rev integer NOT NULL constraint fk_item_types_aud_2_revinfo references revinfo,
    revtype smallint,
    name varchar(255),
    version bigint,
    created_date timestamp without time zone,
    updated_date timestamp without time zone,
    constraint pk_item_types_aud primary key (id, rev)
);

CREATE TABLE items_aud
(
    code varchar(255) NOT NULL,
    rev integer NOT NULL constraint fk_items_aud_2_revinfo references revinfo,
    revtype smallint,
    name varchar(255),
    type_id bigint,
    version bigint,
    created_date timestamp without time zone,
    updated_date timestamp without time zone,
    constraint pk_items_aud primary key (code, rev)
);


