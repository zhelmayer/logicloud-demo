package hu.logicloud.demo.models;

import hu.logicloud.demo.models.items.domain.ItemType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;
import static org.hamcrest.core.Is.is;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ItemsConfig.class)
@TestPropertySource(properties = {"spring.flyway.enabled=false"})
@Transactional
@Commit
public class ItemTypeTest {

    @PersistenceContext
    private EntityManager em;

    @Test
    public void insertTest() {

        ItemType itemType = new ItemType();
        itemType.setName("TYPE_11");
        em.persist(itemType);

        assertThat(itemType, hasProperty("createdDate", is(notNullValue())));
    }

    @Test
    public void deleteTest() {

        ItemType it = new ItemType();
        it.setName("asas1");

        em.persist(it);

        Long itemTypeId = it.getId();

        em.remove(it);

        ItemType deletedType = em.find(ItemType.class, itemTypeId);

        assertThat(deletedType, is(nullValue()));
    }

}
