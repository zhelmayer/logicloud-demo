package hu.logicloud.demo.models;

import hu.logicloud.demo.models.users.domain.User;
import hu.logicloud.demo.models.users.domain.UserSettings;
import hu.logicloud.demo.models.users.domain.UserSettingsId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;
import static org.hamcrest.core.Is.is;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = UsersConfig.class)
@TestPropertySource(properties = {"spring.flyway.enabled=false"})
@Transactional
@Commit
public class UserSettingsTest {

    @PersistenceContext
    private EntityManager em;

    @Test
    public void insertTest() {

        User user = new User();
        user.setName("name");
        user.setLogin("alma");

        em.persist(user);

        UserSettingsId userSettingsId = new UserSettingsId();
        userSettingsId.setUserId(user.getId());
        userSettingsId.setCode("code1");

        UserSettings userSettings = new UserSettings();
        userSettings.setValue("value1");
        userSettings.setUserSettingsId(userSettingsId);

        em.persist(userSettings);

        assertThat(userSettings, hasProperty("createdDate", is(notNullValue())));
    }

    @Test
    public void deleteTest() {

        User user = new User();
        user.setName("name2");
        user.setLogin("alma2");

        em.persist(user);

        UserSettingsId userSettingsId = new UserSettingsId();
        userSettingsId.setUserId(user.getId());
        userSettingsId.setCode("code");

        UserSettings userSettings = new UserSettings();
        userSettings.setValue("value");
        userSettings.setUserSettingsId(userSettingsId);

        em.persist(userSettings);

        UserSettingsId userSettingsIdentity = userSettings.getUserSettingsId();

        em.remove(userSettings);

        UserSettings deletedUserSettings = em.find(UserSettings.class, userSettingsIdentity);

        assertThat(deletedUserSettings, is(nullValue()));
    }

}
