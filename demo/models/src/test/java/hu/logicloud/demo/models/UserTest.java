package hu.logicloud.demo.models;

import hu.logicloud.demo.models.users.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;
import static org.hamcrest.core.Is.is;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = UsersConfig.class)
@TestPropertySource(properties = {"spring.flyway.enabled=false"})
@Transactional
@Commit
public class UserTest {

    @PersistenceContext
    private EntityManager em;

    @Test
    public void insertTest() {

        User user = new User();
        user.setName("name3");
        user.setLogin("alma1");

        em.persist(user);

        assertThat(user, hasProperty("createdDate", is(notNullValue())));
    }

    @Test
    public void deleteTest() {

        User user = new User();
        user.setName("name4");
        user.setLogin("alma4");

        em.persist(user);

        Long userId = user.getId();

        em.remove(user);

        User deletedUser = em.find(User.class, userId);

        assertThat(deletedUser, is(nullValue()));
    }

}
