package hu.logicloud.demo.models;

import hu.logicloud.demo.models.items.domain.Item;
import hu.logicloud.demo.models.items.domain.ItemType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;
import static org.hamcrest.core.Is.is;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ItemsConfig.class)
@TestPropertySource(properties = {"spring.flyway.enabled=false"})
@Transactional
@Commit
public class ItemTest {

    @PersistenceContext
    private EntityManager em;

    @Test
    public void insertTest() {

        Item item = new Item();
        item.setName("name");
        item.setCode("code");

        ItemType itemType = new ItemType();
        itemType.setName("TYPE_1");
        em.persist(itemType);

        item.setItemType(itemType);

        em.persist(item);

        assertThat(item, hasProperty("createdDate", is(notNullValue())));
    }

    @Test
    public void deleteTest() {

        Item item = new Item();

        item.setCode("DELE");
        item.setName("DELE_12235");

        ItemType it = new ItemType();
        it.setName("asas");

        em.persist(it);

        item.setItemType(it);

        em.persist(item);

        String itemCode = item.getCode();

        em.remove(item);

        Item deletedBox = em.find(Item.class, itemCode);

        assertThat(deletedBox, is(nullValue()));
    }

}
