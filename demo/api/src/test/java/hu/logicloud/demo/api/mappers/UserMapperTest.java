package hu.logicloud.demo.api.mappers;

import hu.logicloud.demo.api.models.dtos.UserDTO;
import hu.logicloud.demo.models.users.domain.User;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;

public class UserMapperTest {

    private GenericMapper mapper;

    @Before
    public void setUp() {
        this.mapper = new GenericMapper();
    }

    @Test
    public void toDtoWithEmptyFrom() {
        User entity = new User();
        UserDTO dto = mapper.convert(entity, UserDTO.class);

        assertThat(dto, is(notNullValue()));
        assertThat(dto.getId(), is(nullValue()));
        assertThat(dto.getName(), is(nullValue()));
        assertThat(dto.getLogin(), is(nullValue()));
    }

    @Test
    public void toEntityWithEmptyFrom() {
        UserDTO dto = new UserDTO();
        User entity = mapper.convert(dto, User.class);

        assertThat(entity, is(notNullValue()));
        assertThat(entity.getId(), is(nullValue()));
        assertThat(entity.getName(), is(nullValue()));
        assertThat(entity.getLogin(), is(nullValue()));
        assertThat(entity.getSettings(), is(nullValue()));
    }

    @Test
    public void toDtoWithValidFrom() {
        User entity = new User();
        entity.setId(1L);
        entity.setName("name");
        entity.setLogin("login");

        UserDTO dto = mapper.convert(entity, UserDTO.class);

        assertThat(dto, is(notNullValue()));
        assertThat(dto.getId(), is(1L));
        assertThat(dto.getName(), is("name"));
        assertThat(dto.getLogin(), is("login"));
    }

    @Test
    public void toEntityWithValidFrom() {

        UserDTO dto = new UserDTO();
        dto.setId(1L);
        dto.setName("name");
        dto.setLogin("login");

        User entity = mapper.convert(dto, User.class);

        assertThat(entity, is(notNullValue()));
        assertThat(entity.getId(), is(1L));
        assertThat(entity.getName(), is("name"));
        assertThat(entity.getLogin(), is("login"));
    }
}
