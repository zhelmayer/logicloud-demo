package hu.logicloud.demo.api.mappers;

import hu.logicloud.demo.api.models.dtos.UserSettingsDTO;
import hu.logicloud.demo.api.models.dtos.UserSettingsIdDTO;
import hu.logicloud.demo.models.users.domain.User;
import hu.logicloud.demo.models.users.domain.UserSettings;
import hu.logicloud.demo.models.users.domain.UserSettingsId;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;

public class UserSettingsMapperTest {

    private GenericMapper mapper;

    @Before
    public void setUp() {
        this.mapper = new GenericMapper();
    }

    @Test
    public void toDtoWithEmptyFrom() {
        UserSettings entity = new UserSettings();
        UserSettingsDTO dto = mapper.convert(entity, UserSettingsDTO.class);

        assertThat(dto, is(notNullValue()));
        assertThat(dto.getUserSettingsId(), is(nullValue()));
        assertThat(dto.getValue(), is(nullValue()));
    }

    @Test
    public void toEntityWithEmptyFrom() {
        UserSettingsDTO dto = new UserSettingsDTO();
        UserSettings entity = mapper.convert(dto, UserSettings.class);

        assertThat(entity, is(notNullValue()));
        assertThat(entity.getUserSettingsId(), is(nullValue()));
        assertThat(entity.getValue(), is(nullValue()));
        assertThat(entity.getUser(), is(nullValue()));
    }

    @Test
    public void toDtoWithValidFrom() {
        UserSettings entity = new UserSettings();
        entity.setUserSettingsId(new UserSettingsId());
        entity.setUser(new User());
        entity.setValue("value");

        UserSettingsDTO dto = mapper.convert(entity, UserSettingsDTO.class);

        assertThat(dto, is(notNullValue()));
        assertThat(dto.getUserSettingsId(), is(notNullValue()));
        assertThat(dto.getValue(), is("value"));
    }

    @Test
    public void toEntityWithValidFrom() {
        UserSettingsDTO dto = new UserSettingsDTO();
        dto.setUserSettingsId(new UserSettingsIdDTO());
        dto.setValue("value");

        UserSettings entity = mapper.convert(dto, UserSettings.class);

        assertThat(entity, is(notNullValue()));
        assertThat(entity.getUserSettingsId(), is(notNullValue()));
        assertThat(entity.getValue(), is("value"));
    }
}
