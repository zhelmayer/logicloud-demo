package hu.logicloud.demo.api.mappers;

import hu.logicloud.demo.api.models.dtos.ItemDTO;
import hu.logicloud.demo.api.models.dtos.ItemTypeDTO;
import hu.logicloud.demo.models.items.domain.Item;
import hu.logicloud.demo.models.items.domain.ItemType;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;

public class ItemMapperTest {

    private GenericMapper mapper;

    @Before
    public void setUp() {
        this.mapper = new GenericMapper();
    }

    @Test
    public void toDtoWithEmptyFrom() {
        Item entity = new Item();
        ItemDTO dto = mapper.convert(entity, ItemDTO.class);

        assertThat(dto, is(notNullValue()));
        assertThat(dto.getName(), is(nullValue()));
        assertThat(dto.getCode(), is(nullValue()));
        assertThat(dto.getItemType(), is(nullValue()));
    }

    @Test
    public void toEntityWithEmptyFrom() {
        ItemDTO dto = new ItemDTO();
        Item entity = mapper.convert(dto, Item.class);

        assertThat(entity, is(notNullValue()));
        assertThat(entity.getName(), is(nullValue()));
        assertThat(entity.getCode(), is(nullValue()));
        assertThat(entity.getItemType(), is(nullValue()));
    }

    @Test
    public void toDtoWithValidFrom() {
        Item entity = new Item();
        entity.setName("name");
        entity.setCode("code");
        entity.setItemType(new ItemType());

        ItemDTO dto = mapper.convert(entity, ItemDTO.class);

        assertThat(dto, is(notNullValue()));
        assertThat(dto.getName(), is("name"));
        assertThat(dto.getCode(), is("code"));
        assertThat(dto.getItemType(), is(notNullValue()));
    }

    @Test
    public void toEntityWithValidFrom() {
        ItemDTO dto = new ItemDTO();
        dto.setName("name");
        dto.setCode("code");
        dto.setItemType(new ItemTypeDTO());

        Item entity = mapper.convert(dto, Item.class);

        assertThat(entity, is(notNullValue()));
        assertThat(entity.getName(), is("name"));
        assertThat(entity.getCode(), is("code"));
        assertThat(entity.getItemType(), is(notNullValue()));
    }
}
