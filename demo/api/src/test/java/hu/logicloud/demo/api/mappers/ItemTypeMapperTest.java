package hu.logicloud.demo.api.mappers;

import hu.logicloud.demo.api.models.dtos.ItemTypeDTO;
import hu.logicloud.demo.models.items.domain.ItemType;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;

public class ItemTypeMapperTest {

    private GenericMapper mapper;

    @Before
    public void setUp() {
        this.mapper = new GenericMapper();
    }

    @Test
    public void toDtoWithEmptyFrom() {
        ItemType entity = new ItemType();
        ItemTypeDTO dto = mapper.convert(entity, ItemTypeDTO.class);

        assertThat(dto, is(notNullValue()));
        assertThat(dto.getName(), is(nullValue()));
        assertThat(dto.getId(), is(nullValue()));
    }

    @Test
    public void toEntityWithEmptyFrom() {
        ItemTypeDTO dto = new ItemTypeDTO();
        ItemType entity = mapper.convert(dto, ItemType.class);

        assertThat(entity, is(notNullValue()));
        assertThat(entity.getName(), is(nullValue()));
        assertThat(entity.getId(), is(nullValue()));
    }

    @Test
    public void toDtoWithValidFrom() {
        ItemType entity = new ItemType();
        entity.setId(1L);
        entity.setName("name");

        ItemTypeDTO dto = mapper.convert(entity, ItemTypeDTO.class);

        assertThat(dto, is(notNullValue()));
        assertThat(dto.getId(), is(1L));
        assertThat(dto.getName(), is("name"));
    }

    @Test
    public void toEntityWithValidFrom() {
        ItemTypeDTO dto = new ItemTypeDTO();
        dto.setName("name");
        dto.setId(1L);

        ItemType entity = mapper.convert(dto, ItemType.class);

        assertThat(entity, is(notNullValue()));
        assertThat(entity.getName(), is("name"));
        assertThat(entity.getId(), is(1L));
    }
}
