package hu.logicloud.demo.api.services;

import hu.logicloud.demo.api.exceptions.AlreadyExistException;
import hu.logicloud.demo.api.exceptions.NotFoundException;
import hu.logicloud.demo.api.models.metamodels.ItemType_;
import hu.logicloud.demo.api.models.pojos.ItemTypeGroupPOJO;
import hu.logicloud.demo.api.repositories.items.ItemTypeRepository;
import hu.logicloud.demo.api.services.interfaces.ItemTypeService;
import hu.logicloud.demo.models.items.domain.Item;
import hu.logicloud.demo.models.items.domain.ItemType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.text.MessageFormat;
import java.util.List;


@Service
@Transactional
public class ItemTypeServiceImpl implements ItemTypeService {

    private static final Logger logger = LoggerFactory.getLogger(ItemTypeServiceImpl.class);

    private final ItemTypeRepository itemTypeRepository;
    private final EntityManager entityManager;

    @Autowired
    public ItemTypeServiceImpl(ItemTypeRepository itemTypeRepository, @Qualifier("itemsEntityManagerFactory") EntityManager entityManager) {
        this.itemTypeRepository = itemTypeRepository;
        this.entityManager = entityManager;
    }

    public List<ItemType> get(){
        return itemTypeRepository.findAll();
    }

    public ItemType get(Long id){

        return itemTypeRepository.findById(id)
            .orElseThrow(() -> new NotFoundException(MessageFormat.format("ItemType id: {0} Not found.", id)));
    }

    public List<ItemTypeGroupPOJO> getItemTypeGroups(){

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<ItemTypeGroupPOJO> query = cb.createQuery(ItemTypeGroupPOJO.class);

        Root<ItemType> itemTypes = query.from(ItemType.class);
        Join<ItemType, Item> items = itemTypes.join(ItemType_.ITEMS, JoinType.LEFT);
        query.multiselect(
                itemTypes.get(ItemType_.NAME), cb.count(items)
        ).groupBy(
                itemTypes.get(ItemType_.NAME)
        );

        TypedQuery<ItemTypeGroupPOJO> typedQuery = entityManager.createQuery(query);

        return typedQuery.getResultList();
    }

    public ItemType save(ItemType itemType){

        if(itemType.getId() != null){
            itemTypeRepository.findById(itemType.getId())
                    .ifPresent(s -> {
                        throw new AlreadyExistException(MessageFormat.format("ItemType is already exist with id: {0}.", itemType.getId()));
                    });
        }

        return itemTypeRepository.save(itemType);
    }

    public ItemType update(Long id, ItemType newItemType){

        return itemTypeRepository.findById(id)
                .map(itemType -> {
                    itemType.setName(newItemType.getName());
                    return itemTypeRepository.save(itemType);
                }).orElseThrow(() -> new NotFoundException(MessageFormat.format("Item id: {0} Not found.", id)));
    }

    public boolean delete(Long id){

        try {
            itemTypeRepository.deleteById(id);
            return true;
        }catch(Exception ex){
            logger.error(ex.getMessage());
            return false;
        }
    }
}
