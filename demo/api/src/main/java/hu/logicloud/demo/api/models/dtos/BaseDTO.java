package hu.logicloud.demo.api.models.dtos;

import lombok.Data;

import java.util.Date;

@Data
public class BaseDTO {
    private Long version;
    private Date createdDate;
    private Date updatedDate;
}
