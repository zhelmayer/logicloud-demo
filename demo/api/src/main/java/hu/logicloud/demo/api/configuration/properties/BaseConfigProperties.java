package hu.logicloud.demo.api.configuration.properties;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.boot.model.relational.Database;
import org.hibernate.cfg.AvailableSettings;
import org.springframework.beans.factory.BeanClassLoaderAware;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateSettings;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.jdbc.DataSourceInitializationMode;
import org.springframework.boot.jdbc.DatabaseDriver;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy;
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.sql.DataSource;
import java.nio.charset.Charset;
import java.util.*;
import java.util.function.Supplier;

@Data
public class BaseConfigProperties implements BeanClassLoaderAware, InitializingBean {

    private static final String DISABLED_SCANNER_CLASS = "org.hibernate.boot.archive.scan.internal.DisabledScanner";

    private final Naming naming = new Naming();
    private String ddlAuto;
    private Boolean useNewIdGeneratorMappings;

    private String migrationFilesLocation;

    private Map<String, String> properties = new HashMap<>();
    private final List<String> mappingResources = new ArrayList<>();
    private String databasePlatform;
    private Database database;
    private boolean generateDdl = false;
    private boolean showSql = false;
    private Boolean openInView;

    private ClassLoader classLoader;
    private String name;
    private boolean generateUniqueName;
    private Class<? extends DataSource> type;
    private String driverClassName;
    private String url;
    private String username;
    private String password;
    private String jndiName;

    private DataSourceInitializationMode initializationMode = DataSourceInitializationMode.EMBEDDED;

    private String platform = "all";
    private List<String> schema;
    private String schemaUsername;
    private String schemaPassword;
    private List<String> data;
    private String dataUsername;
    private String dataPassword;
    private boolean continueOnError = false;
    private String separator = ";";
    private Charset sqlScriptEncoding;
    private EmbeddedDatabaseConnection embeddedDatabaseConnection = EmbeddedDatabaseConnection.NONE;
    private Xa xa = new Xa();
    private String uniqueName;

    public Map<String, Object> determineHibernateProperties(Map<String, String> jpaProperties, HibernateSettings settings) {
        Assert.notNull(jpaProperties, "JpaProperties must not be null");
        Assert.notNull(settings, "Settings must not be null");
        return getAdditionalProperties(jpaProperties, settings);
    }

    private Map<String, Object> getAdditionalProperties(Map<String, String> existing, HibernateSettings settings) {

        Map<String, Object> result = new HashMap<>(existing);
        applyNewIdGeneratorMappings(result);
        applyScanner(result);
        getNaming().applyNamingStrategies(result);
        String ddlAuto = determineDdlAuto(existing, settings::getDdlAuto);

        if (StringUtils.hasText(ddlAuto) && !"none".equals(ddlAuto)) {
            result.put(AvailableSettings.HBM2DDL_AUTO, ddlAuto);
        }
        else {
            result.remove(AvailableSettings.HBM2DDL_AUTO);
        }

        Collection<HibernatePropertiesCustomizer> customizers = settings.getHibernatePropertiesCustomizers();
        if (!ObjectUtils.isEmpty(customizers)) {
            customizers.forEach((customizer) -> customizer.customize(result));
        }
        return result;
    }

    private void applyNewIdGeneratorMappings(Map<String, Object> result) {

        if (this.useNewIdGeneratorMappings != null) {
            result.put(AvailableSettings.USE_NEW_ID_GENERATOR_MAPPINGS, this.useNewIdGeneratorMappings.toString());
        }
        else if (!result.containsKey(AvailableSettings.USE_NEW_ID_GENERATOR_MAPPINGS)) {
            result.put(AvailableSettings.USE_NEW_ID_GENERATOR_MAPPINGS, "true");
        }
    }

    private void applyScanner(Map<String, Object> result) {

        if (!result.containsKey(AvailableSettings.SCANNER) && ClassUtils.isPresent(DISABLED_SCANNER_CLASS, null)) {
            result.put(AvailableSettings.SCANNER, DISABLED_SCANNER_CLASS);
        }
    }

    private String determineDdlAuto(Map<String, String> existing, Supplier<String> defaultDdlAuto) {

        String ddlAuto = existing.get(AvailableSettings.HBM2DDL_AUTO);

        if (ddlAuto != null) {
            return ddlAuto;
        }
        return (this.ddlAuto != null) ? this.ddlAuto : defaultDdlAuto.get();
    }

    @Data
    public static class Naming {

        private String implicitStrategy;

        private String physicalStrategy;

        private void applyNamingStrategies(Map<String, Object> properties) {

            applyNamingStrategy(properties, AvailableSettings.IMPLICIT_NAMING_STRATEGY, this.implicitStrategy,
                    SpringImplicitNamingStrategy.class.getName());
            applyNamingStrategy(properties, AvailableSettings.PHYSICAL_NAMING_STRATEGY, this.physicalStrategy,
                    SpringPhysicalNamingStrategy.class.getName());
        }

        private void applyNamingStrategy(Map<String, Object> properties, String key, Object strategy,
                                         Object defaultStrategy) {
            if (strategy != null) {
                properties.put(key, strategy);
            }
            else if (defaultStrategy != null && !properties.containsKey(key)) {
                properties.put(key, defaultStrategy);
            }
        }
    }

    @Override
    public void setBeanClassLoader(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        this.embeddedDatabaseConnection = EmbeddedDatabaseConnection.get(this.classLoader);
    }

    public DataSourceBuilder<?> initializeDataSourceBuilder() {
        return DataSourceBuilder.create(getClassLoader()).type(getType()).driverClassName(determineDriverClassName())
                .url(determineUrl()).username(determineUsername()).password(determinePassword());
    }

    public String determineDriverClassName() {

        if (StringUtils.hasText(this.driverClassName)) {
            Assert.state(driverClassIsLoadable(), () -> "Cannot load driver class: " + this.driverClassName);
            return this.driverClassName;
        }
        String driverClassName = null;
        if (StringUtils.hasText(this.url)) {
            driverClassName = DatabaseDriver.fromJdbcUrl(this.url).getDriverClassName();
        }
        if (!StringUtils.hasText(driverClassName)) {
            driverClassName = this.embeddedDatabaseConnection.getDriverClassName();
        }
        if (!StringUtils.hasText(driverClassName)) {
            throw new DataSourceBeanCreationException("Failed to determine a suitable driver class", this,
                    this.embeddedDatabaseConnection);
        }
        return driverClassName;
    }

    private boolean driverClassIsLoadable() {

        try {
            ClassUtils.forName(this.driverClassName, null);
            return true;
        }
        catch (UnsupportedClassVersionError ex) {
            throw ex;
        }
        catch (Throwable ex) {
            return false;
        }
    }

    public String determineUrl() {

        if (StringUtils.hasText(this.url)) {
            return this.url;
        }
        String databaseName = determineDatabaseName();
        String url = (databaseName != null) ? this.embeddedDatabaseConnection.getUrl(databaseName) : null;
        if (!StringUtils.hasText(url)) {
            throw new DataSourceBeanCreationException("Failed to determine suitable jdbc url", this,
                    this.embeddedDatabaseConnection);
        }
        return url;
    }

    public String determineDatabaseName() {

        if (this.generateUniqueName) {
            if (this.uniqueName == null) {
                this.uniqueName = UUID.randomUUID().toString();
            }
            return this.uniqueName;
        }
        if (StringUtils.hasLength(this.name)) {
            return this.name;
        }
        if (this.embeddedDatabaseConnection != EmbeddedDatabaseConnection.NONE) {
            return "testdb";
        }
        return null;
    }

    public String determineUsername() {

        if (StringUtils.hasText(this.username)) {
            return this.username;
        }
        if (EmbeddedDatabaseConnection.isEmbedded(determineDriverClassName())) {
            return "sa";
        }
        return null;
    }

    public String determinePassword() {

        if (StringUtils.hasText(this.password)) {
            return this.password;
        }
        if (EmbeddedDatabaseConnection.isEmbedded(determineDriverClassName())) {
            return "";
        }
        return null;
    }

    @Data
    public static class Xa {
        private String dataSourceClassName;
        private Map<String, String> properties = new LinkedHashMap<>();
    }

    @Data
    @EqualsAndHashCode(callSuper = false)
    static class DataSourceBeanCreationException extends BeanCreationException {

        private final BaseConfigProperties properties;

        private final EmbeddedDatabaseConnection connection;

        DataSourceBeanCreationException(String message, BaseConfigProperties properties,
                                        EmbeddedDatabaseConnection connection) {
            super(message);
            this.properties = properties;
            this.connection = connection;
        }
    }
}
