package hu.logicloud.demo.api.repositories.users;

import hu.logicloud.demo.models.users.domain.UserSettings;
import hu.logicloud.demo.models.users.domain.UserSettingsId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserSettingsRepository  extends JpaRepository<UserSettings, UserSettingsId> {
}