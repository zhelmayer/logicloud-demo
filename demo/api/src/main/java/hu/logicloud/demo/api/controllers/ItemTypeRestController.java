package hu.logicloud.demo.api.controllers;

import hu.logicloud.demo.api.mappers.GenericMapper;
import hu.logicloud.demo.api.models.pojos.ItemTypeGroupPOJO;
import hu.logicloud.demo.api.services.interfaces.ItemTypeService;
import hu.logicloud.demo.models.items.domain.ItemType;
import hu.logicloud.demo.api.models.dtos.ItemTypeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/item-type-management/item-types")
public class ItemTypeRestController {

    private final ItemTypeService itemTypeService;
    private final GenericMapper mapper;

    @Autowired
    public ItemTypeRestController(ItemTypeService itemTypeService, GenericMapper genericMapper) {
        this.itemTypeService = itemTypeService;
        this.mapper = genericMapper;
    }

    @GetMapping
    public List<ItemTypeDTO> getItemTypes(){

        List<ItemType> itemTypesList = itemTypeService.get();

        return itemTypesList.stream()
                .map(x -> this.mapper.convert(x, ItemTypeDTO.class))
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/{id}")
    public ItemTypeDTO getItemType(@PathVariable Long id){
        ItemType itemType = itemTypeService.get(id);

        return itemType == null
                ? null
                : this.mapper.convert(itemType, ItemTypeDTO.class);
    }

    @GetMapping(value = "/get-item-type-groups")
    public List<ItemTypeGroupPOJO> getItemTypeGroups(){
        return itemTypeService.getItemTypeGroups();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ItemTypeDTO postItemType(@RequestBody ItemTypeDTO itemTypesDTO){

        ItemType itemTypes = this.mapper.convert(itemTypesDTO, ItemType.class);
        return this.mapper.convert(itemTypeService.save(itemTypes), ItemTypeDTO.class);
    }

    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ItemTypeDTO putItemType(@PathVariable Long id, @RequestBody ItemTypeDTO itemTypesDTO){

        ItemType itemType = this.mapper.convert(itemTypesDTO, ItemType.class);
        return this.mapper.convert(itemTypeService.update(id, itemType), ItemTypeDTO.class);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public boolean deleteItemType(@PathVariable Long id){
        return itemTypeService.delete(id);
    }
}