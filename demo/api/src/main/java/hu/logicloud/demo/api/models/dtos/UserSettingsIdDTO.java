package hu.logicloud.demo.api.models.dtos;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@ApiModel(value = "UserSettingsId")
@EqualsAndHashCode
public class UserSettingsIdDTO implements Serializable {
    private Long id;
    private String code;
}
