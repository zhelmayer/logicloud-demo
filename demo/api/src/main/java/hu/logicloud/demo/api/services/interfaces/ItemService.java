package hu.logicloud.demo.api.services.interfaces;

import hu.logicloud.demo.models.items.domain.Item;

import java.util.List;

public interface ItemService {

    List<Item> get();

    Item get(String code);

    boolean delete(Item items);

    Item update(String code, Item item);

    Item save(Item items);
}
