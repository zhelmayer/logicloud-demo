package hu.logicloud.demo.api.models.pojos;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ItemTypeGroupPOJO {
    private String type;
    private Long count;

}
