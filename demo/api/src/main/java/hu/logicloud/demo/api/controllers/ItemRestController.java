package hu.logicloud.demo.api.controllers;

import hu.logicloud.demo.api.mappers.GenericMapper;
import hu.logicloud.demo.api.services.interfaces.ItemService;
import hu.logicloud.demo.models.items.domain.Item;
import hu.logicloud.demo.api.models.dtos.ItemDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/item-management/items")
public class ItemRestController {

    private final ItemService itemService;
    private final GenericMapper mapper;

    @Autowired
    public ItemRestController(ItemService itemService, GenericMapper genericMapper) {
        this.itemService = itemService;
        this.mapper = genericMapper;
    }

    @GetMapping
    public List<ItemDTO> getItems(){

        List<Item> itemList = itemService.get();

        return itemList.stream()
                .map(x -> this.mapper.convert(x, ItemDTO.class))
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/{code}")
    public ItemDTO getItem(@PathVariable String code){
        Item item = itemService.get(code);

        return item == null
                ? null
                : this.mapper.convert(item, ItemDTO.class);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ItemDTO postItem(@RequestBody ItemDTO itemsDTO){

        Item item = this.mapper.convert(itemsDTO, Item.class);
        return this.mapper.convert(itemService.save(item), ItemDTO.class);
    }

    @PutMapping(value = "/{code}")
    @ResponseStatus(HttpStatus.OK)
    public ItemDTO putItem(@PathVariable String code, @RequestBody ItemDTO itemsDTO){

        Item items = this.mapper.convert(itemsDTO, Item.class);
        return this.mapper.convert(itemService.update(code, items), ItemDTO.class);
    }

    @DeleteMapping(value = "/{code}")
    @ResponseStatus(HttpStatus.OK)
    public boolean deleteItem(@PathVariable String code){

        Item item = itemService.get(code);
        return itemService.delete(item);
    }
}
