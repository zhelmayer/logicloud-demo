package hu.logicloud.demo.api.configuration.properties;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Primary;

@Primary
@ConfigurationProperties("items.datasource")
public class ItemsConfigProperties extends BaseConfigProperties {
}
