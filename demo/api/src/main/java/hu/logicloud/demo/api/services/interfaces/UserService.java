package hu.logicloud.demo.api.services.interfaces;

import hu.logicloud.demo.models.users.domain.User;

import java.util.List;

public interface UserService {

    List<User> get();

    User get(Long id);

    boolean delete(Long id);

    User update(Long id, User user);

    User save(User user);
}
