package hu.logicloud.demo.api.services;

import hu.logicloud.demo.api.exceptions.AlreadyExistException;
import hu.logicloud.demo.api.exceptions.NotFoundException;
import hu.logicloud.demo.models.items.domain.Item;
import hu.logicloud.demo.api.repositories.items.ItemRepository;
import hu.logicloud.demo.api.services.interfaces.ItemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.MessageFormat;
import java.util.List;

@Service
@Transactional
public class ItemServiceImpl implements ItemService {

    private static final Logger logger = LoggerFactory.getLogger(ItemServiceImpl.class);

    private final ItemRepository itemRepository;

    @Autowired
    public ItemServiceImpl(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public List<Item> get(){
        return itemRepository.findAll();
    }

    public Item get(String code){
        return itemRepository.findById(code)
                .orElseThrow(() -> new NotFoundException(MessageFormat.format("Item id: {0} Not found.", code)));
    }

    public Item save(Item item){

        if(item.getCode() != null){
            itemRepository.findById(item.getCode())
                    .ifPresent(s -> {
                        throw new AlreadyExistException(MessageFormat.format("Item is already exist with code: {0}.", item.getCode()));
                    });
        }

        return itemRepository.save(item);
    }

    public Item update(String code, Item newItem){

        return itemRepository.findById(code)
                .map(item -> {
                    item.setItemType(newItem.getItemType());
                    item.setName(newItem.getName());
                    return itemRepository.save(item);
                }).orElseThrow(() -> new NotFoundException(MessageFormat.format("Item id: {0} Not found.", code)));
    }

    public boolean delete(Item item){

        try {
            itemRepository.delete(item);
            return true;
        }catch (Exception ex){
            logger.error(ex.getMessage());
            return false;
        }
    }
}
