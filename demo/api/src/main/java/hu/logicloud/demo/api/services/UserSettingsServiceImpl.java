package hu.logicloud.demo.api.services;

import hu.logicloud.demo.api.exceptions.AlreadyExistException;
import hu.logicloud.demo.api.exceptions.NotFoundException;
import hu.logicloud.demo.api.repositories.users.UserSettingsRepository;
import hu.logicloud.demo.api.services.interfaces.UserSettingsService;
import hu.logicloud.demo.models.users.domain.UserSettings;
import hu.logicloud.demo.models.users.domain.UserSettingsId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.util.List;

@Service
@Transactional
public class UserSettingsServiceImpl implements UserSettingsService {

    private static final Logger logger = LoggerFactory.getLogger(UserSettingsServiceImpl.class);

    private final UserSettingsRepository userSettingsRepository;

    @Autowired
    public UserSettingsServiceImpl(UserSettingsRepository userSettingsRepository) {
        this.userSettingsRepository = userSettingsRepository;
    }

    public List<UserSettings> get(){
        return userSettingsRepository.findAll();
    }

    public UserSettings get(UserSettingsId userSettingsId){

        return userSettingsRepository.findById(userSettingsId)
            .orElseThrow(() -> new NotFoundException(MessageFormat.format("UserSettings id: {0}, {1} Not found.", userSettingsId.getUserId(), userSettingsId.getCode())));
    }

    public UserSettings save(UserSettings userSettings){

        if(userSettings.getUserSettingsId() != null){

            userSettingsRepository.findById(userSettings.getUserSettingsId())
                    .ifPresent(s -> {
                        throw new AlreadyExistException(
                                MessageFormat.format("UserSettings is already exist with id: {0} {1}.",
                                        userSettings.getUserSettingsId().getUserId(),
                                        userSettings.getUserSettingsId().getCode())
                        );
                    });
        }

        return userSettingsRepository.save(userSettings);
    }

    public UserSettings update(UserSettingsId userSettingsId, UserSettings userSettings){

        return userSettingsRepository.findById(userSettingsId)
                .map(newUserSettings -> {
                    newUserSettings.setValue(userSettings.getValue());
                    return userSettingsRepository.save(newUserSettings);
                }).orElseThrow(() -> new NotFoundException(MessageFormat.format("UserSettings id: {0}, {1} Not found.", userSettingsId.getUserId(), userSettingsId.getCode())));
    }

    public boolean delete(UserSettings userSettings){

        try {
            userSettingsRepository.delete(userSettings);
            return true;
        }catch (Exception ex){
            logger.error(ex.getMessage());
            return false;
        }
    }
}
