package hu.logicloud.demo.api.services;

import hu.logicloud.demo.api.exceptions.AlreadyExistException;
import hu.logicloud.demo.api.exceptions.NotFoundException;
import hu.logicloud.demo.api.repositories.users.UserRepository;
import hu.logicloud.demo.api.services.interfaces.UserService;
import hu.logicloud.demo.models.users.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> get(){
        return userRepository.findAll();
    }

    public User get(Long id){

        return userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(MessageFormat.format("User id: {0} Not found.", id)));
    }

    public User save(User user){

        if(user.getId() != null){
            userRepository.findById(user.getId())
                    .ifPresent(s -> {
                        throw new AlreadyExistException(MessageFormat.format("User is already exist with id: {0}.", user.getId()));
                    });
        }

        return userRepository.save(user);
    }

    public User update(Long id, User user){

        return userRepository.findById(id)
                .map(newUser -> {
                    newUser.setLogin(user.getLogin());
                    newUser.setName(user.getName());
                    return userRepository.save(newUser);
                }).orElseThrow(() -> new NotFoundException(MessageFormat.format("User id: {0} Not found.", id)));
    }

    public boolean delete(Long id){

        try {
            userRepository.deleteById(id);
            return true;
        }catch (Exception ex){
            logger.error(ex.getMessage());
            return false;
        }
    }
}
