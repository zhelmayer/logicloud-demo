package hu.logicloud.demo.api.configuration;

import hu.logicloud.demo.api.configuration.properties.ItemsConfigProperties;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;


@Configuration
@EnableJpaRepositories(
        entityManagerFactoryRef = "itemsEntityManagerFactory",
        transactionManagerRef = "itemsTransactionManager",
        basePackages = "hu.logicloud.demo.api.repositories.items"
)
@EnableTransactionManagement
@EnableConfigurationProperties(ItemsConfigProperties.class)
public class ItemsDbConfig {

    @Primary
    @Bean(name = "itemsDataSource")
    public DataSource itemsDataSource(ItemsConfigProperties itemsConfigProperties) {

        FlywayUtil.migrate(itemsConfigProperties);

        return DataSourceBuilder
                .create()
                .url(itemsConfigProperties.getUrl())
                .username(itemsConfigProperties.getUsername())
                .password(itemsConfigProperties.getPassword())
                .driverClassName(itemsConfigProperties.getDriverClassName())
                .build();
    }

    @Primary
    @Bean(name = "itemsEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean itemsEntityManagerFactory(EntityManagerFactoryBuilder builder, @Qualifier("itemsDataSource") DataSource itemsDataSource) {

        return builder
                .dataSource(itemsDataSource)
                .packages("hu.logicloud.demo.models.items.domain")
                .persistenceUnit("items")
                .build();
    }

    @Primary
    @Bean(name = "itemsTransactionManager")
    public PlatformTransactionManager itemsTransactionManager(@Qualifier("itemsEntityManagerFactory") EntityManagerFactory itemsEntityManagerFactory) {
        return new JpaTransactionManager(itemsEntityManagerFactory);
    }
}