package hu.logicloud.demo.api.models.dtos;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@ApiModel(value = "User")
@EqualsAndHashCode(callSuper=false)
public class UserDTO extends BaseDTO{

    private Long id;
    private String login;
    private String name;
}
