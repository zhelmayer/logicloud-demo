package hu.logicloud.demo.api.services.interfaces;

import hu.logicloud.demo.models.users.domain.UserSettings;
import hu.logicloud.demo.models.users.domain.UserSettingsId;

import java.util.List;

public interface UserSettingsService {

    List<UserSettings> get();

    UserSettings get(UserSettingsId userSettingsId);

    boolean delete(UserSettings userSettings);

    UserSettings update(UserSettingsId userSettingsId, UserSettings userSettings);

    UserSettings save(UserSettings userSettings);
}
