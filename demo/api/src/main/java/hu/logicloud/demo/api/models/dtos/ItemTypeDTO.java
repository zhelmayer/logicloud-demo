package hu.logicloud.demo.api.models.dtos;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@ApiModel(value = "ItemTypes")
@EqualsAndHashCode(callSuper = false)
public class ItemTypeDTO extends BaseDTO {

    private Long id;
    private String name;
}
