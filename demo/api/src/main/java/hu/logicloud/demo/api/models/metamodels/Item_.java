package hu.logicloud.demo.api.models.metamodels;

import hu.logicloud.demo.models.items.domain.Item;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

@StaticMetamodel(Item.class)
public abstract class Item_ {

    public static volatile SingularAttribute<Item, String> code;
    public static volatile SingularAttribute<Item, Integer> itemType;
    public static volatile SingularAttribute<Item, String> name;
    public static volatile SingularAttribute<Item, Integer> version;
    public static volatile SingularAttribute<Item, Date> createdDate;
    public static volatile SingularAttribute<Item, Date> updatedDate;

    public static final String CODE = "code";
    public static final String ITEM_TYPE = "itemType";
    public static final String NAME = "name";
    public static final String VERSION = "version";
    public static final String CREATED_DATE = "createdDate";
    public static final String UPLOAD_DATE = "uploadDate";
}