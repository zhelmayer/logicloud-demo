package hu.logicloud.demo.api.services.interfaces;

import hu.logicloud.demo.api.models.pojos.ItemTypeGroupPOJO;
import hu.logicloud.demo.models.items.domain.ItemType;

import java.util.List;

public interface ItemTypeService {

    List<ItemType> get();

    ItemType get(Long id);

    List<ItemTypeGroupPOJO> getItemTypeGroups();

    boolean delete(Long id);

    ItemType update(Long id, ItemType itemTypes);

    ItemType save(ItemType items);
}
