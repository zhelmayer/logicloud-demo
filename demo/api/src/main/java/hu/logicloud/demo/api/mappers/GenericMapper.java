package hu.logicloud.demo.api.mappers;

import ma.glasnost.orika.MapperFacade;
import org.springframework.stereotype.Service;

@Service
public class GenericMapper extends BaseMapper{

    public <T, G> T convert(G g, Class<T> t) {

        modelMapper.classMap(g.getClass(), t);
        MapperFacade mapper = modelMapper.getMapperFacade();
        return mapper.map(g, t);
    }

}
