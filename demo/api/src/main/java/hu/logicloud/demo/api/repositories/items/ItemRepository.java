package hu.logicloud.demo.api.repositories.items;

import hu.logicloud.demo.models.items.domain.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends JpaRepository<Item, String> {
}


