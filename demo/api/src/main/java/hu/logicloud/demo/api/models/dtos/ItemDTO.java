package hu.logicloud.demo.api.models.dtos;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@ApiModel(value = "Item")
@EqualsAndHashCode(callSuper=false)
public class ItemDTO extends BaseDTO {

    private String code;
    private String name;
    private ItemTypeDTO itemType;
}
