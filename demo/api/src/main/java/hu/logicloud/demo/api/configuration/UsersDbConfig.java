package hu.logicloud.demo.api.configuration;

import hu.logicloud.demo.api.configuration.properties.UsersConfigProperties;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories(
        entityManagerFactoryRef = "usersEntityManagerFactory",
        transactionManagerRef = "usersTransactionManager",
        basePackages = "hu.logicloud.demo.api.repositories.users"
)
@EnableTransactionManagement
@EnableConfigurationProperties(UsersConfigProperties.class)
public class UsersDbConfig {


    @Bean(name = "usersDataSource")
    public DataSource usersDataSource(UsersConfigProperties usersConfigProperties) {

        FlywayUtil.migrate(usersConfigProperties);

        return DataSourceBuilder
                .create()
                .url(usersConfigProperties.getUrl())
                .username(usersConfigProperties.getUsername())
                .password(usersConfigProperties.getPassword())
                .driverClassName(usersConfigProperties.getDriverClassName())
                .build();
    }

    @Bean(name = "usersEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean usersEntityManagerFactory(EntityManagerFactoryBuilder builder, @Qualifier("usersDataSource") DataSource usersDataSource) {

        return
                builder
                        .dataSource(usersDataSource)
                        .packages("hu.logicloud.demo.models.users.domain")
                        .persistenceUnit("users")
                        .build();
    }

    @Bean(name = "usersTransactionManager")
    public PlatformTransactionManager usersTransactionManager(@Qualifier("usersEntityManagerFactory") EntityManagerFactory usersEntityManagerFactory) {
        return new JpaTransactionManager(usersEntityManagerFactory);
    }
}