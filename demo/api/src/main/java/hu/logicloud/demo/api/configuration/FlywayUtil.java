package hu.logicloud.demo.api.configuration;

import hu.logicloud.demo.api.configuration.properties.BaseConfigProperties;
import org.apache.tomcat.util.buf.StringUtils;
import org.flywaydb.core.Flyway;

public class FlywayUtil {

    private FlywayUtil(){}

    public static void migrate(BaseConfigProperties baseConfigProperties){

        final Flyway flyway = Flyway
                .configure()
                .dataSource(
                        baseConfigProperties.getUrl(),
                        baseConfigProperties.getUsername(),
                        baseConfigProperties.getPassword()
                )
                .schemas(StringUtils.join(baseConfigProperties.getSchema(), ','))
                .locations(baseConfigProperties.getMigrationFilesLocation())
                .load();

        flyway.migrate();
    }

}
