package hu.logicloud.demo.api.mappers;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

public abstract class BaseMapper {

    protected final MapperFactory modelMapper = new DefaultMapperFactory.Builder().build();
}
