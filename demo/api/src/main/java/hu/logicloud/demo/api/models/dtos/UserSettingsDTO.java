package hu.logicloud.demo.api.models.dtos;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@ApiModel(value = "UserSettings")
@EqualsAndHashCode(callSuper=false)
public class UserSettingsDTO extends BaseDTO {

    private UserSettingsIdDTO userSettingsId;
    private String value;

}
