package hu.logicloud.demo.api.controllers;

import hu.logicloud.demo.api.mappers.GenericMapper;
import hu.logicloud.demo.api.models.dtos.UserSettingsDTO;
import hu.logicloud.demo.api.services.interfaces.UserSettingsService;
import hu.logicloud.demo.models.users.domain.UserSettings;
import hu.logicloud.demo.models.users.domain.UserSettingsId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/user-settings-management/settings")
public class UserSettingsRestController {

    private final UserSettingsService userSettingsService;
    private final GenericMapper mapper;

    @Autowired
    public UserSettingsRestController(UserSettingsService userSettingsService, GenericMapper mapper) {
        this.userSettingsService = userSettingsService;
        this.mapper = mapper;
    }

    @GetMapping
    public List<UserSettingsDTO> getSettings(){

        List<UserSettings> userSettingsList = userSettingsService.get();

        return userSettingsList.stream()
                .map(x -> this.mapper.convert(x, UserSettingsDTO.class))
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/{id}/{code}")
    public UserSettingsDTO getSetting(@PathVariable Long id, @PathVariable String code){

        UserSettingsId userSettingsId = new UserSettingsId();
        userSettingsId.setUserId(id);
        userSettingsId.setCode(code);

        UserSettings userSettings = userSettingsService.get(userSettingsId);

        return userSettings == null
                ? null
                : this.mapper.convert(userSettings, UserSettingsDTO.class);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UserSettingsDTO postSetting(@RequestBody UserSettingsDTO userSettingsDTO){

        UserSettings userSettings = this.mapper.convert(userSettingsDTO, UserSettings.class);
        return this.mapper.convert(userSettingsService.save(userSettings), UserSettingsDTO.class);
    }

    @PutMapping(value = "/{id}/{code}")
    @ResponseStatus(HttpStatus.OK)
    public UserSettingsDTO putSetting(@PathVariable Long id, @PathVariable String code, @RequestBody UserSettingsDTO userSettingsDTO){

        UserSettings userSettings = this.mapper.convert(userSettingsDTO, UserSettings.class);

        UserSettingsId userSettingsId = new UserSettingsId();
        userSettingsId.setUserId(id);
        userSettingsId.setCode(code);

        return this.mapper.convert(userSettingsService.update(userSettingsId, userSettings), UserSettingsDTO.class);
    }

    @DeleteMapping(value = "/{id}/{code}")
    @ResponseStatus(HttpStatus.OK)
    public boolean deleteSetting(@PathVariable Long id, @PathVariable String code){

        UserSettingsId userSettingsId = new UserSettingsId();
        userSettingsId.setUserId(id);
        userSettingsId.setCode(code);

        return userSettingsService.delete(userSettingsService.get(userSettingsId));
    }
}
