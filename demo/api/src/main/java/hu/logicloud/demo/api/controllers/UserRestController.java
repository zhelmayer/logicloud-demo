package hu.logicloud.demo.api.controllers;

import hu.logicloud.demo.api.mappers.GenericMapper;
import hu.logicloud.demo.api.models.dtos.UserDTO;
import hu.logicloud.demo.api.services.interfaces.UserService;
import hu.logicloud.demo.models.users.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/user-management/users")
public class UserRestController {

    private final UserService userService;
    private final GenericMapper mapper;

    @Autowired
    public UserRestController(UserService userService, GenericMapper mapper) {
        this.userService = userService;
        this.mapper = mapper;
    }

    @GetMapping
    public List<UserDTO> getUsers(){

        List<User> userList = userService.get();

        return userList.stream()
                .map(x -> this.mapper.convert(x, UserDTO.class))
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/{id}")
    public UserDTO getUser(@PathVariable Long id){

        User user = userService.get(id);
        return this.mapper.convert(user, UserDTO.class);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UserDTO postUser(@RequestBody UserDTO userDTO){

        User user = this.mapper.convert(userDTO, User.class);
        return this.mapper.convert(userService.save(user), UserDTO.class);
    }

    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public UserDTO putUser(@PathVariable Long id, @RequestBody UserDTO userDTO){

        User user = this.mapper.convert(userDTO, User.class);
        return this.mapper.convert(userService.update(id, user), UserDTO.class);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public boolean deleteUser(@PathVariable Long id){
        return userService.delete(id);
    }
}
