package hu.logicloud.demo.api.configuration.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("users.datasource")
public class UsersConfigProperties extends BaseConfigProperties{
}
