package hu.logicloud.demo.api.models.metamodels;

import hu.logicloud.demo.models.items.domain.Item;
import hu.logicloud.demo.models.items.domain.ItemType;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Collection;

import java.util.Date;

@StaticMetamodel(ItemType.class)
public abstract class ItemType_ {

    public static volatile SingularAttribute<ItemType, Integer> id;
    public static volatile SingularAttribute<ItemType, String> name;
    public static volatile SingularAttribute<ItemType, Integer> version;
    public static volatile SingularAttribute<ItemType, Date> createdDate;
    public static volatile SingularAttribute<ItemType, Date> updatedDate;
    public static volatile SingularAttribute<ItemType, Collection<Item>> items;

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String VERSION = "version";
    public static final String CREATED_DATE = "createdDate";
    public static final String UPLOAD_DATE = "uploadDate";
    public static final String ITEMS = "items";
}